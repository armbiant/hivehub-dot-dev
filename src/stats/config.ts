import { RouteRecordRaw } from 'vue-router'

const name = 'stats'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/stats',
    name: 'stats',
    component: () => import('~/stats/pages/Stats.vue')
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_STATS === 'true' ? true : false, // disabled by default
  routes: routes
}
