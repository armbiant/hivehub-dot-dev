interface IDataPoint {
  time: number
  date: string
  open: number
  close: number
  high: number
  low: number
  volume?: number
}

/**
 * Parse data to tradingview chart format with maximum resolution.
 * @param data
 * @returns
 */
export const parseChartData = (data: any): Array<IDataPoint> => {
  const parsed: Array<IDataPoint> = []
  let lastPrice = -1

  for (const trade of data) {
    const timestamp = new Date(trade.date).getTime()
    const date = trade.date
    const hive = trade.current_pays.includes('HIVE') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    const hbd = trade.current_pays.includes('HBD') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    const hivePrice = hbd / hive

    // Filter out steep changes
    if (lastPrice == -1) {
      lastPrice = hivePrice
    }
    if (hivePrice / lastPrice > 1.15 || hivePrice / lastPrice < 0.9) {
      lastPrice = hivePrice
      continue
    }

    parsed.push({
      time: timestamp,
      date: date,
      open: hivePrice,
      close: hivePrice,
      high: hivePrice,
      low: hivePrice,
      volume: 1
    })
  }

  return parsed
}

/**
 * Parse data to tradingview chart format using a specific resolution.
 * @param data
 * @param resolution resolution in minutes
 * @returns
 */
export const parseChartDataResolution = (data: any, resolution: number): Array<IDataPoint> => {
  const parsed: Array<IDataPoint> = []
  let lastParsed: IDataPoint = { time: 0, open: -1, close: -1, high: -1, low: -1 }

  resolution *= 1000 * 60

  for (const trade of data) {
    const timestamp = new Date(trade.date).getTime()
    const hive = trade.current_pays.includes('HIVE') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    const hbd = trade.current_pays.includes('HBD') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    const hivePrice = hbd / hive

    if (timestamp > lastParsed.time + resolution * 1000 * 60) {
      if (lastParsed.time > 0) {
        parsed.push(lastParsed)
      }
      lastParsed = {
        time: timestamp,
        open: hivePrice,
        close: hivePrice,
        high: hivePrice,
        low: hivePrice
      }
    } else {
      lastParsed.high = Math.max(lastParsed.high, hivePrice)
      lastParsed.low = Math.min(lastParsed.low, hivePrice)
    }
  }

  return parsed
}
