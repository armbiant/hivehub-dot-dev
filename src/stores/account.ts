import { Account } from '@hiveio/dhive'
import { acceptHMRUpdate, defineStore } from 'pinia'
import { Ref } from 'vue'
import { getAccounts, getOpenOrders } from '~/common/api/hiveApi'

type AccountData = {
  name: string | null
  authenticated: boolean
  balance: string
  hbd_balance: string
  open_orders: Array<object>
}

export const useAccountStore = defineStore('account', () => {
  const loggedInAccount = localStorage.getItem('users')
  const account: Ref<AccountData> = loggedInAccount
    ? ref({ name: loggedInAccount, authenticated: true, balance: '0 HIVE', hbd_balance: '0 HBD', open_orders: [] })
    : ref({ name: null, authenticated: false, balance: '0 HIVE', hbd_balance: '0 HBD', open_orders: [] })

  const updateOpenOrders = async () => {
    if (account.value.name) {
      account.value.open_orders = await getOpenOrders([account.value.name])
    }
  }

  const updateBalances = async () => {
    if (account.value.name) {
      const accountData = await getAccounts([account.value.name]).then((result: [Account]) => result[0])
      account.value.balance = `${accountData.balance}`
      account.value.hbd_balance = `${accountData.hbd_balance}`
    }
  }

  if (loggedInAccount) {
    updateOpenOrders()
    updateBalances()
  }

  const authenticate = async (user: string) =>
    new Promise((resolve, reject) => {
      if (!window.hive_keychain) {
        return reject('Keychain not found')
      }

      // reset values
      account.value.name = null
      account.value.authenticated = false
      account.value.balance = '0 HIVE'
      account.value.hbd_balance = '0 HBD'
      account.value.open_orders = []

      window.hive_keychain.requestSignBuffer(user, `${user}${Date.now()}`, 'Active', async function (result: any) {
        if (result.error) {
          return reject(result.error)
        }

        account.value.name = user
        account.value.authenticated = true
        localStorage.setItem('users', account.value.name)

        updateBalances()

        return resolve(account.value)
      })
    })

  const signOut = () => {
    account.value.name = null
    account.value.authenticated = false
    account.value.balance = '0 HIVE'
    account.value.hbd_balance = '0 HBD'
    account.value.open_orders = []

    localStorage.removeItem('users')
  }

  return {
    updateOpenOrders,
    updateBalances,
    authenticate,
    signOut,
    account
  }
})

if (import.meta.hot) import.meta.hot.accept(acceptHMRUpdate(useAccountStore, import.meta.hot))
