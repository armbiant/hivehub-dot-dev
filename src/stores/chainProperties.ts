import { DynamicGlobalProperties } from '@hiveio/dhive'
import { Ref } from 'vue'
import { acceptHMRUpdate, defineStore } from 'pinia'
import { getDynamicGlobalProperties, getHardforkVersion } from '~/common/api/hiveApi'

export const useChainPropertiesStore = defineStore('chainProperties', () => {
  const dynamicGlobalProperties: Ref<DynamicGlobalProperties | undefined> = ref()
  const hardforkVersion: Ref<string | undefined> = ref()

  let isLoadingDynamicGlobalProperties = false

  const refreshDynamicGlobalProperties = async () => {
    if (isLoadingDynamicGlobalProperties) {
      return
    }

    if (dynamicGlobalProperties.value) {
      const elapsed = Date.now() - new Date(dynamicGlobalProperties.value.time).getTime()
      if (elapsed <= 10000) {
        return
      }
    }

    try {
      isLoadingDynamicGlobalProperties = true
      dynamicGlobalProperties.value = await getDynamicGlobalProperties()
    } finally {
      isLoadingDynamicGlobalProperties = false
    }

    return dynamicGlobalProperties.value
  }

  let isLoadingHardforkVersion = false
  const refreshHardforkVersion = async () => {
    if (!isLoadingHardforkVersion && !hardforkVersion.value) {
      try {
        isLoadingHardforkVersion = true
        hardforkVersion.value = await getHardforkVersion()
      } finally {
        isLoadingHardforkVersion = false
      }
    }

    return hardforkVersion.value
  }

  return {
    dynamicGlobalProperties,
    hardforkVersion,
    refreshDynamicGlobalProperties,
    refreshHardforkVersion
  }
})

if (import.meta.hot) import.meta.hot.accept(acceptHMRUpdate(useChainPropertiesStore, import.meta.hot))
