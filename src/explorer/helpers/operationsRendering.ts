import { Operation } from '@hiveio/dhive'
import appsConfig from '~/explorer/helpers/apps'
import { splitAndCapitalize, formatCurrency } from '~/common/helpers/formatter'
import HiveIcon from '~/assets/images/icons/hive.svg'
import { parseAmount } from '~/common/helpers/hiveFunctions'
import { useChainPropertiesStore } from '~/stores/chainProperties'

const defaultOpTypeClass = 'font-medium'
const defaultImgClass = 'inline-block h-5 w-5 rounded-full border border-gray-300 dark:border-slate-700 mr-1'
const defaultLinkClass = 'font-medium text-emerald-600 dark:text-emerald-400'
const defaultShiftedImgClass = 'inline-block h-5 w-5 rounded-full border border-gray-300 dark:border-slate-700'

// Types
type RenderElement = {
  tag: string
  text?: string
  attributes?: {
    src?: string
    to?: string
    href?: string
    target?: string
    class?: string
  }
}

type RenderRecord = {
  icon?: string
  elements?: RenderElement[]
  shifted?: RenderElement[]
  details?: string[]
  indented?: boolean
}

// Helper functions
const chainPropertiesStore = useChainPropertiesStore()

const dynamicGlobalProperties = computed(() => chainPropertiesStore.dynamicGlobalProperties)
const totalVestingShares = computed(() => parseAmount(dynamicGlobalProperties.value && dynamicGlobalProperties.value.total_vesting_shares))
const totalVestingFundHive = computed(() => parseAmount(dynamicGlobalProperties.value && dynamicGlobalProperties.value.total_vesting_fund_hive))

const vestToHive = (vestingShares: string) => {
  const hp = totalVestingFundHive.value * (parseFloat(vestingShares) / totalVestingShares.value)
  return `${formatCurrency(hp, '0,0.000')} HP`
}

const handleNaiNotation = (amount: string, nai: string) => {
  const naiNotation = {
    HBD: '@@000000013',
    HIVE: '@@000000021',
    VESTS: '@@000000037'
  }
  for (const currency in naiNotation) {
    if (nai === naiNotation[currency]) return `${(+amount / 1000).toFixed(3)} ${currency}`
  }
}

// Rendering functions
const renderSeparator = (sep: string = '|') => ({
  tag: 'span',
  text: sep,
  attributes: { class: 'text-gray-500 dark:text-gray-300' }
})

const renderDefaultType = (opName: string, text?: string) => {
  return [
    {
      tag: 'span',
      text: text || splitAndCapitalize(opName),
      attributes: { class: defaultOpTypeClass }
    }
  ]
}

const renderDefaultAccount = (account: string) => {
  return [
    renderSeparator(),
    {
      tag: 'img',
      attributes: { src: `https://images.hive.blog/u/${account}/avatar/small`, class: defaultImgClass }
    },
    {
      tag: 'router-link',
      text: account,
      attributes: { to: `/@${account}`, class: defaultLinkClass }
    }
  ]
}

const renderShiftedAccount = (account: string, separator = true) => {
  const rightSeparator = separator
    ? [
        {
          tag: 'span',
          text: ' | ',
          attributes: { class: ' text-sm' }
        }
      ]
    : []

  return [
    {
      tag: 'img',
      attributes: { src: `https://images.hive.blog/u/${account}/avatar/small`, class: defaultShiftedImgClass }
    },
    {
      tag: 'router-link',
      text: account,
      attributes: { to: `/@${account}`, class: `${defaultLinkClass} text-sm` }
    },
    ...rightSeparator
  ]
}

export const render = (op: Operation): RenderRecord => {
  const record = op[0] === 'custom_json' ? renderCustomJson(op) : renderOperation(op)

  return {
    icon: record.icon || HiveIcon,
    elements: record.elements,
    shifted: record.shifted,
    details: record.details || [],
    indented: record.indented
  }
}

const renderOperation = (op: Operation): RenderRecord => {
  const name = op[0]
  const payload = op[1]

  // operations
  switch (name) {
    //A
    case 'account_create':
      return {
        elements: [...renderDefaultType(name, 'Create account'), ...renderDefaultAccount(payload.creator)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.new_account_name}`,
            attributes: { to: `/@${payload.new_account_name}`, class: `${defaultLinkClass} text-sm` }
          }
        ]
      }
    case 'account_created':
      return {
        elements: [...renderDefaultType(name, 'Account created')],
        shifted: [...renderShiftedAccount(payload.new_account_name, false)]
      }
    case 'account_update':
      return {
        elements: [...renderDefaultType(name, 'Update Active Metadata'), ...renderDefaultAccount(payload.account)]
      }
    case 'account_update2':
      return {
        elements: [...renderDefaultType(name, 'Update Active Metadata (2)'), ...renderDefaultAccount(payload.account)]
      }
    case 'account_witness_proxy':
      return {
        elements: [...renderDefaultType(name, 'Set Witness Proxy'), ...renderDefaultAccount(payload.account)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.proxy}`,
            attributes: { to: `/@${payload.proxy}`, class: `${defaultLinkClass} text-sm` }
          }
        ]
      }
    case 'account_witness_vote':
      return {
        elements: payload.approve
          ? [...renderDefaultType(name, 'Witness Vote'), ...renderDefaultAccount(payload.account)]
          : [...renderDefaultType(name, 'Remove Witness Vote'), ...renderDefaultAccount(payload.account)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.witness}`,
            attributes: { to: `/@${payload.witness}`, class: `${defaultLinkClass} text-sm` }
          }
        ]
      }
    case 'author_reward':
      return {
        elements: [...renderDefaultType(name, 'Reward Pool: Author Reward')],
        shifted: [
          ...renderShiftedAccount(payload.author),
          {
            tag: 'span',
            text: ['hive_payout', 'hbd_payout', 'vesting_payout']
              .filter(k => payload[k] && parseFloat(payload[k]))
              .map(k => {
                const [amount, token] = payload[k].split(' ')
                return token === 'VESTS' ? vestToHive(amount) : `${parseFloat(amount).toFixed(3)} ${token}`
              })
              .join(', '),
            attributes: { class: 'text-sm' }
          }
        ]
      }

    // C
    case 'cancel_transfer_from_savings':
      return {
        elements: [...renderDefaultType(name, 'Cancel Savings Transfer'), ...renderDefaultAccount(payload.from)]
        //     shifted: [
        //   {
        //     tag: 'a',
        //     text: truncate(`@${trx.transaction_id.slice(0, 8)}`),
        //     attributes: { href: `/tx/${trx.transaction_id}`, class: `${defaultLinkClass} text-sm` }
        //   }
        // ]
      }
    case 'change_recovery_account':
      return {
        elements: [...renderDefaultType(name, 'Change Recovery Account')],
        shifted: [
          ...renderShiftedAccount(payload.account_to_recover, false),
          { tag: 'br' },
          {
            tag: 'router-link',
            text: `@${payload.new_recovery_account}`,
            attributes: { to: `/@${payload.new_recovery_account}`, class: `${defaultLinkClass} text-sm` }
          }
        ]
      }
    case 'changed_recovery_account':
      return {
        elements: [...renderDefaultType(name, 'Recovery Account Changed'), ...renderDefaultAccount(payload.old_recovery_account)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.new_recovery_account}`,
            attributes: { to: `/@${payload.new_recovery_account}`, class: `${defaultLinkClass} text-sm` }
          }
        ]
      }
    case 'claim_account':
      return {
        elements: [...renderDefaultType(name, 'Claim Account Tokens'), ...renderDefaultAccount(payload.creator)]
      }
    case 'claim_reward_balance':
      return {
        elements: [...renderDefaultType(name, 'Reward Pool: Claim Balance'), ...renderDefaultAccount(payload.account)],
        shifted: [
          {
            tag: 'span',
            text: ['reward_hive', 'reward_hbd', 'reward_vests']
              .filter(k => payload[k] && parseFloat(payload[k]))
              .map(k => {
                const [amount, token] = payload[k].split(' ')
                return token === 'VESTS' ? vestToHive(amount) : `${parseFloat(amount).toFixed(3)} ${token}`
              })
              .join(', '),
            attributes: { class: 'text-sm' }
          }
        ]
      }
    case 'collateralized_convert':
      return {
        elements: [...renderDefaultType(name, 'Convert HIVE to HBD'), ...renderDefaultAccount(payload.owner)],
        shifted: [
          {
            tag: 'span',
            text: payload.amount,
            attributes: { class: 'text-sm' }
          }
        ]
      }
    case 'comment':
      return {
        elements: [...renderDefaultType(name, 'Comment'), ...renderDefaultAccount(payload.author)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.parent_author} /`,
            attributes: {
              to: `/@${payload.parent_author}`,
              class: `${defaultLinkClass} text-sm`
            }
          },
          {
            tag: 'a',
            text: ` ${payload.parent_permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.parent_author}/${payload.parent_permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }
    // const commentElements: RenderElement[] = [
    //   {
    //     tag: 'img',
    //     attributes: { src: `https://images.hive.blog/u/${payload.author}/avatar/small`, class: defaultImgClass }
    //   },
    //   {
    //     tag: 'router-link',
    //     text: payload.author,
    //     attributes: { to: `/@${payload.author}`, class: defaultLinkClass }
    //   }
    // ]
    // return {
    //   elements: !payload.parent_author
    //     ? commentElements.concat([
    //         {
    //           tag: 'span',
    //           text: `published a post`
    //         },
    //         {
    //           tag: 'a',
    //           text: payload.permlink,
    //           attributes: { href: `https://peakd.com/@${payload.author}/${payload.permlink}`, class: defaultLinkClass }
    //         },
    //         {
    //           tag: 'span',
    //           text: 'in'
    //         },
    //         {
    //           tag: 'a',
    //           text: payload.parent_permlink,
    //           attributes: { href: `https://peakd.com/${payload.parent_permlink}`, class: defaultLinkClass }
    //         }
    //       ])
    //     : commentElements.concat([
    //         {
    //           tag: 'span',
    //           text: `replied to`
    //         },
    //         {
    //           tag: 'a',
    //           text: payload.parent_permlink,
    //           attributes: { href: `https://peakd.com/@${payload.parent_author}/${payload.parent_permlink}`, class: defaultLinkClass }
    //         },
    //         {
    //           tag: 'span',
    //           text: 'by'
    //         },
    //         {
    //           tag: 'router-link',
    //           text: payload.parent_author,
    //           attributes: { to: `/@${payload.parent_author}`, class: defaultLinkClass }
    //         }
    //       ])
    // }
    case 'comment_benefactor_reward':
      return {
        elements: [...renderDefaultType(name, 'Reward Pool: Beneficiary Reward')],
        shifted: [
          ...renderShiftedAccount(payload.benefactor),
          {
            tag: 'span',
            text: ['hive_payout', 'hbd_payout', 'vesting_payout']
              .filter(k => payload[k] && parseFloat(payload[k]))
              .map(k => {
                const [amount, token] = payload[k].split(' ')
                return token === 'VESTS' ? vestToHive(amount) : `${parseFloat(amount).toFixed(3)} ${token}`
              })
              .join(', '),
            attributes: { class: 'text-sm' }
          },
          { tag: 'br' },
          // second line
          {
            tag: 'router-link',
            text: `@${payload.author} /`,
            attributes: {
              to: `/@${payload.author}`,
              class: `${defaultLinkClass} text-sm`
            }
          },
          {
            tag: 'a',
            text: ` ${payload.permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.author}/${payload.permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }
    case 'comment_payout_update':
      return {
        elements: [...renderDefaultType(name, 'Comment Payout Update')],
        shifted: [
          ...renderShiftedAccount(payload.author, false),
          {
            tag: 'a',
            text: ` / ${payload.permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.author}/${payload.permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }
    case 'comment_reward':
      return {
        elements: [...renderDefaultType(name, 'Reward Pool: Post/Comment Reward')],
        shifted: [
          ...renderShiftedAccount(payload.author),
          {
            tag: 'span',
            text: `${payload.payout}`,
            attributes: { class: 'text-sm' }
          },
          { tag: 'br' },
          // second line
          {
            tag: 'router-link',
            text: `@${payload.author} /`,
            attributes: {
              to: `/@${payload.author}`,
              class: `${defaultLinkClass} text-sm`
            }
          },
          {
            tag: 'a',
            text: ` ${payload.permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.author}/${payload.permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }
    case 'comment_options':
      const commentOptionsElements: RenderElement[] = [
        {
          tag: 'span',
          text: 'Options:'
        }
      ]
      const fields: string[] = ['max_accepted_payout', 'percent_hbd', 'allow_votes', 'allow_curation_rewards']
      return {
        elements: commentOptionsElements.concat(
          fields.map(field => ({
            tag: 'span',
            text: `${field} ${payload[field]}`,
            attributes: { class: 'font-medium' }
          }))
        )
      }
    case 'convert':
      return {
        elements: [...renderDefaultType(name, 'Convert HBD to HIVE'), ...renderDefaultAccount(payload.owner)],
        shifted: [
          {
            tag: 'span',
            text: payload.amount,
            attributes: { class: 'text-sm' }
          }
        ]
      }
    case 'create_claimed_account':
      return {
        elements: [...renderDefaultType(name, 'Create Account (w/ Claim Token)'), ...renderDefaultAccount(payload.creator)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.new_account_name}`,
            attributes: { to: `/@${payload.new_account_name}`, class: `${defaultLinkClass} text-sm` }
          }
        ]
      }
    case 'create_proposal':
      return {
        elements: [...renderDefaultType(name, 'Create Proposal'), ...renderDefaultAccount(payload.creator)],
        shifted: [
          {
            tag: 'a',
            // id isn't returned from the payload
            text: /*`${payload.id} | */ `${payload.permlink} `,
            attributes: {
              href: `https://peakd.com/@${payload.creator}/${payload.permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          },
          {
            tag: 'span',
            text: `| ${payload.daily_pay}`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'curation_reward':
      return {
        elements: [...renderDefaultType(name, 'Reward Pool: Curation Reward')],
        shifted: [
          ...renderShiftedAccount(payload.curator),
          {
            tag: 'span',
            text: vestToHive(payload.reward),
            attributes: { class: 'text-sm' }
          },
          {
            tag: 'br'
          },
          // second line
          {
            tag: 'router-link',
            text: `@${payload.comment_author} /`,
            attributes: {
              to: `/@${payload.comment_author}`,
              class: `${defaultLinkClass} text-sm`
            }
          },
          {
            tag: 'a',
            text: ` ${payload.comment_permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.comment_author}/${payload.comment_permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }
    case 'custom':
      return {
        elements: [...renderDefaultType(name, 'Custom')],
        shifted: [
          {
            tag: 'span',
            text: `#id: ${payload.id} | ${payload.required_auths.join(', ')}`,
            attributes: { class: 'text-sm' }
          }
        ]
      }
    case 'custom_binary':
      return {
        elements: [...renderDefaultType(name, 'Json Binary')]
      }

    // D
    case 'decline_voting_rights':
      return {
        elements: [...renderDefaultType(name, 'Decline Voting Rights'), ...renderDefaultAccount(payload.account)]
      }
    case 'delayed_voting':
      return {
        elements: [...renderDefaultType(name, 'Governance Voting Enabled')],
        shifted: [
          ...renderShiftedAccount(payload.voter),
          {
            tag: 'span',
            text: `votes: ${payload.votes}`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'delegate_vesting_shares':
      return {
        elements: [...renderDefaultType(name, 'Delegate HP'), ...renderDefaultAccount(payload.delegator)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.delegatee} `,
            attributes: { to: `/@${payload.delegatee}`, class: `${defaultLinkClass} text-sm` }
          },
          {
            tag: 'span',
            text: `| ${vestToHive(payload.vesting_shares)}`,
            attributes: { class: `text-sm` }
          }
        ]
      }

    case 'delete_comment':
      return {
        elements: [...renderDefaultType(name, 'Delete Comment'), ...renderDefaultAccount(payload.author)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.author} /`,
            attributes: {
              to: `/@${payload.author}`,
              class: `${defaultLinkClass} text-sm`
            }
          },
          {
            tag: 'a',
            text: ` ${payload.permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.author}/${payload.permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }

    // E
    case 'effective_comment_vote':
      return {
        elements: [...renderDefaultType(name, 'Reward Pool: Applied Vote')],
        shifted: [
          ...renderShiftedAccount(payload.voter),
          {
            tag: 'span',
            text: payload.pending_payout,
            attributes: { class: 'text-sm' }
          },
          {
            tag: 'span',
            text: 'Updated Pending Payout',
            attributes: { class: `block text-xs text-gray-500` }
          }
        ],
        indented: true
      }
    case 'expired_account_notification':
      return {
        elements: [...renderDefaultType(name, 'Expired Governance Vote')],
        shifted: [...renderShiftedAccount(payload.account, false)]
      }

    // F
    case 'feed_publish':
      return {
        elements: [...renderDefaultType(name, 'Witness Price Feed'), ...renderDefaultAccount(payload.publisher)],
        shifted: [
          {
            tag: 'span',
            text: payload.exchange_rate.base,
            attributes: { class: 'text-sm' }
          }
        ]
      }
    case 'fill_collateralized_convert_request':
      return {
        elements: [...renderDefaultType(name, 'HIVE to HBD Convert Complete')],
        shifted: [
          {
            tag: 'span',
            text: `${payload.amount} OUT`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'fill_convert_request':
      return {
        elements: [...renderDefaultType(name, 'HIVE to HBD Conversion Request')],
        shifted: [
          ...renderShiftedAccount(payload.owner),
          {
            tag: 'span',
            text: `${payload.amount_in} IN`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'fill_order':
      return {
        elements: [...renderDefaultType(name, 'Fill Market Purchase')],
        shifted: [
          ...renderShiftedAccount(payload.current_owner, false),
          { tag: 'br' },
          {
            tag: 'router-link',
            text: `@${payload.open_owner} `,
            attributes: { to: `/@${payload.open_owner}`, class: `${defaultLinkClass} text-sm` }
          },
          {
            tag: 'span',
            text: `| ${payload.current_pays} of ${payload.open_pays}`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'fill_recurrent_transfer':
      return {
        elements: [...renderDefaultType(name, 'Recurring Transfer Execution'), ...renderDefaultAccount(payload.from)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.to} `,
            attributes: { to: `/@${payload.to}`, class: `${defaultLinkClass} text-sm` }
          },
          {
            tag: 'span',
            text: `| ${payload.amount}`,
            attributes: { class: `text-sm` }
          },
          // second line
          {
            tag: 'span',
            text: `memo: "${payload.memo}"`,
            attributes: { class: `block text-gray-500 text-xs` }
          }
        ]
      }
    case 'fill_transfer_from_savings':
      return {
        elements: [...renderDefaultType(name, 'Withdraw from Savings Complete')],
        shifted: [
          ...renderShiftedAccount(payload.from, false),
          {
            tag: 'span',
            text: ' to ',
            attributes: { class: 'text-sm' }
          },

          {
            tag: 'router-link',
            text: `@${payload.to} `,
            attributes: { to: `/@${payload.to}`, class: `${defaultLinkClass} text-sm` }
          },
          {
            tag: 'span',
            text: `| ${payload.amount.amount}`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'fill_vesting_withdraw':
      const receiverTags =
        payload.from_account !== payload.to_account
          ? [
              {
                tag: 'span',
                text: 'to ',
                attributes: { class: 'text-sm' }
              },
              ...renderShiftedAccount(payload.to_account, false)
            ]
          : []

      return {
        elements: [...renderDefaultType(name, 'Unstake HIVE Execution')],
        shifted: [
          ...renderShiftedAccount(payload.from_account, false),
          ...receiverTags,
          {
            tag: 'span',
            text: `| ${payload.deposited}`,
            attributes: { class: `text-sm` }
          }
        ]
      }

    // H
    case 'hardfork':
      return {
        elements: [...renderDefaultType(name, 'HardFork Schedule')]
        // shifted: [
        //   ...renderShiftedAccount(payload.owner),
        //   {
        //     tag: 'span',
        //     text: `${payload.interest}`,
        //     attributes: { class: `text-sm` }
        //   }
        // ]
      }

    // I
    case 'interest':
      return {
        elements: [...renderDefaultType(name, 'HBD Interest Payment')],
        shifted: [
          ...renderShiftedAccount(payload.owner),
          {
            tag: 'span',
            text: `${payload.interest}`,
            attributes: { class: `text-sm` }
          }
        ]
      }

    // L
    case 'limit_order_cancel':
      return {
        elements: [...renderDefaultType(name, 'Cancel Internal Market Order'), ...renderDefaultAccount(payload.owner)],
        shifted: [
          {
            tag: 'span',
            text: `#id: ${payload.orderid}`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'limit_order_create':
      return {
        elements: [...renderDefaultType(name, 'Create Internal Market Order'), ...renderDefaultAccount(payload.owner)],
        shifted: [
          {
            tag: 'span',
            text: `${payload.amount_to_sell} ⇄ ${payload.min_to_receive}`,
            attributes: { class: `text-sm` }
          },
          // second line
          {
            tag: 'span',
            text: `#id: "${payload.orderid}"`,
            attributes: { class: `block text-gray-500 text-xs` }
          }
        ]
      }
    case 'limit_order_create2':
      return {
        elements: [...renderDefaultType(name, 'Create Internal Market Order (2)'), ...renderDefaultAccount(payload.owner)],
        shifted: [
          {
            tag: 'span',
            text: `${payload.amount_to_sell} at ${payload.exchange_rate.quote}`,
            attributes: { class: `text-sm` }
          },
          // second line
          {
            tag: 'span',
            text: `#id: "${payload.orderid}"`,
            attributes: { class: `block text-gray-500 text-xs` }
          }
        ]
      }

    // P
    case 'producer_reward':
      return {
        elements: [...renderDefaultType(name, 'Witness Reward')],
        shifted: [
          ...renderShiftedAccount(payload.producer),
          {
            tag: 'span',
            text: `${vestToHive(payload.vesting_shares)}`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'proposal_pay':
      return {
        elements: [...renderDefaultType(name, 'Proposal Payment')],
        shifted: [
          ...renderShiftedAccount(payload.receiver),
          {
            tag: 'span',
            text: payload.payment,
            attributes: { class: `text-sm` }
          }
        ]
      }

    // R
    case 'recover_account':
      return {
        elements: [...renderDefaultType(name, 'Confirm Account Recovery'), ...renderDefaultAccount(payload.account_to_recover)]
      }

    case 'recurrent_transfer':
      return payload.amount
        ? {
            elements: [...renderDefaultType(name, 'Start Recurrent Transfer'), ...renderDefaultAccount(payload.from)],
            shifted: [
              {
                tag: 'router-link',
                text: `@${payload.to} `,
                attributes: { to: `/@${payload.to}`, class: `${defaultLinkClass} text-sm` }
              },
              {
                tag: 'span',
                text: `| ${payload.recurrence}x, ${payload.amount}`,
                attributes: { class: `text-sm` }
              }
            ]
          }
        : {
            elements: [...renderDefaultType(name, 'Stop Recurrent Transfer'), ...renderDefaultAccount(payload.from)]
          }
    case 'remove_proposal':
      return {
        elements: [...renderDefaultType(name, 'Remove Proposal'), ...renderDefaultAccount(payload.creator)],
        shifted: [
          {
            tag: 'span',
            text: payload.proposal_ids,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'request_account_recovery':
      return {
        elements: [...renderDefaultType(name, 'Start Account Recovery'), ...renderDefaultAccount(payload.recovery_account)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.account_to_recover}`,
            attributes: { to: `/@${payload.account_to_recover}`, class: `${defaultLinkClass} text-sm` }
          }
        ]
      }
    case 'return_vesting_delegation':
      return {
        elements: [...renderDefaultType(name, 'Return Vesting Delegation')],
        shifted: [
          ...renderShiftedAccount(payload.account),
          {
            tag: 'span',
            text: vestToHive(payload.vesting_shares),
            attributes: { class: `text-sm` }
          }
        ]
      }

    //S
    case 'set_withdraw_vesting_rule':
      return {
        elements: [...renderDefaultType(name, 'Change Unstaking Route'), ...renderDefaultAccount(payload.from_account)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.to_account}`,
            attributes: { to: `/@${payload.to_account}`, class: `${defaultLinkClass} text-sm` }
          }
        ]
      }
    case 'shutdown_witness':
      return {
        elements: [...renderDefaultType(name, 'Witness Shutdown')],
        shifted: [...renderDefaultAccount(payload.owner)]
      }
    case 'sps_convert':
      return {
        elements: [...renderDefaultType(name, 'Proposal Funds Conversion (ninja)')],
        shifted: [
          ...renderShiftedAccount(payload.fund_account),
          {
            tag: 'span',
            text: `From ${payload.hive_amount_in} to ${payload.hbd_amount_out}`,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'sps_fund':
      return {
        elements: [...renderDefaultType(name, 'Proposal Funds Inflation')],
        shifted: [
          {
            tag: 'span',
            text: payload.additional_funds,
            attributes: { class: `text-sm` }
          }
        ]
      }

    // T
    case 'transfer':
      return {
        elements: [...renderDefaultType(name, 'Transfer Token'), ...renderDefaultAccount(payload.from)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.to} `,
            attributes: { to: `/@${payload.to}`, class: `${defaultLinkClass} text-sm` }
          },
          {
            tag: 'span',
            text: `| ${payload.amount}`,
            attributes: { class: `text-sm` }
          },
          // second line
          {
            tag: 'span',
            text: `memo: "${payload.memo}"`,
            attributes: { class: `block text-gray-500 text-xs` }
          }
        ]
      }
    // const transferElements: RenderElement[] = [
    //   {
    //     tag: 'img',
    //     attributes: { src: `https://images.hive.blog/u/${payload.from}/avatar/small`, class: defaultImgClass }
    //   },
    //   {
    //     tag: 'router-link',
    //     text: payload.from,
    //     attributes: { to: `/@${payload.from}`, class: defaultLinkClass }
    //   },
    //   {
    //     tag: 'span',
    //     text: `transfer to`
    //   },
    //   {
    //     tag: 'img',
    //     attributes: { src: `https://images.hive.blog/u/${payload.to}/avatar/small`, class: defaultImgClass }
    //   },
    //   {
    //     tag: 'router-link',
    //     text: payload.to,
    //     attributes: { to: `/@${payload.to}`, class: defaultLinkClass }
    //   }
    // ]

    // return {
    //   elements: payload.memo
    //     ? transferElements.concat([
    //         {
    //           tag: 'span',
    //           text: 'with memo:',
    //           attributes: { class: 'text-gray-500' }
    //         },
    //         {
    //           tag: 'code',
    //           text: payload.memo,
    //           attributes: { class: 'text-gray-500 text-ellipsis overflow-hidden' }
    //         }
    //       ])
    //     : transferElements
    // }

    case 'transfer_from_savings':
      return {
        elements: [...renderDefaultType(name, 'Withdraw from Savings'), ...renderDefaultAccount(payload.to)],
        shifted: [
          {
            tag: 'span',
            text: payload.amount,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'transfer_to_savings':
      return {
        elements: [...renderDefaultType(name, 'Transfer to Savings'), ...renderDefaultAccount(payload.from)],
        shifted: [
          {
            tag: 'span',
            text: payload.amount,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'transfer_to_vesting':
      return {
        elements: [...renderDefaultType(name, 'Stake HIVE'), ...renderDefaultAccount(payload.from)],
        shifted: [
          {
            tag: 'span',
            text: payload.amount,
            attributes: { class: `text-sm` }
          }
        ]
      }
    case 'transfer_to_vesting_completed':
      const shiftedAccountTags = payload.from_account !== payload.to_account ? renderShiftedAccount(payload.to_account) : []
      return {
        elements: [...renderDefaultType(name, 'Stake HIVE Execution')],
        shifted: [
          ...shiftedAccountTags,
          {
            tag: 'span',
            text: payload.hive_vested.replace('HIVE', 'HP'),
            attributes: { class: `text-sm` }
          }
        ]
      }

    // U
    case 'update_proposal':
      return {
        elements: [...renderDefaultType(name, 'Update Proposal'), ...renderDefaultAccount(payload.from)],
        shifted: [
          {
            tag: 'a',
            text: `${payload.proposal_id} / ${payload.permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.creator}/${payload.permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }

    // V
    case 'vote':
      return {
        elements: [
          ...renderDefaultType(name, 'Reward Pool: Vote'),
          {
            tag: 'span',
            text: `(${payload.weight / 100}%)`
          },
          ...renderDefaultAccount(payload.voter)
        ],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.author} /`,
            attributes: {
              to: `/@${payload.author}`,
              class: `${defaultLinkClass} text-sm`
            }
          },
          {
            tag: 'a',
            text: ` ${payload.permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.author}/${payload.permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }
    case 'vote2':
      return {
        elements: [...renderDefaultType(name, 'Reward Pool: Vote (2)'), ...renderDefaultAccount(payload.voter)],
        shifted: [
          {
            tag: 'router-link',
            text: `@${payload.author} /`,
            attributes: {
              to: `/@${payload.author}`,
              class: `${defaultLinkClass} text-sm`
            }
          },
          {
            tag: 'a',
            text: ` ${payload.permlink}`,
            attributes: {
              href: `https://peakd.com/@${payload.author}/${payload.permlink}`,
              target: '_blank',
              class: `${defaultLinkClass} text-sm`
            }
          }
        ]
      }

    // W
    case 'withdraw_vesting':
      return {
        elements: [...renderDefaultType(name, 'Unstake Hive'), ...renderDefaultAccount(payload.account)],
        shifted: [
          {
            tag: 'span',
            text: vestToHive(payload.vesting_shares),
            attributes: { class: 'text-sm' }
          }
        ]
      }
    case 'witness_set_properties':
      return {
        elements: [...renderDefaultType(name, 'Witness Properties'), ...renderDefaultAccount(payload.owner)]
      }
    case 'witness_update':
      return {
        elements: [...renderDefaultType(name, 'Start Witness'), ...renderDefaultAccount(payload.owner)]
      }

    default:
      return {
        icon: HiveIcon,
        elements: [...renderDefaultType(name)],
        shifted: []
      }
  }
}

const renderCustomJson = (op: Operation): RenderRecord => {
  if (op[0] !== 'custom_json') {
    throw new Error(`Cannot render ${op[0]} as custom_json`)
  }

  const name = op[0]
  const payload = op[1]
  const customJsonAccount = payload.required_auths.length ? payload.required_auths[0] : payload.required_posting_auths[0]

  let json = null
  try {
    json = JSON.parse(payload.json)
  } catch (parsingError) {
    console.error('Invalid json in custom json transaction: ', parsingError)

    return {
      icon: HiveIcon,
      elements: [
        {
          tag: 'span',
          text: `Bad Formatted Json`,
          attributes: { class: 'text-red-500' }
        },
        ...renderDefaultType(name, `(${payload.id})`),
        ...renderDefaultAccount(customJsonAccount)
      ],
      ...(payload.required_auths.length ? { details: ['Auth: Active'] } : {})
    }
  }

  const app = appsConfig.find(app => app.match(op))
  const { icon, text } = app ? { icon: app.icon, text: app.name } : { icon: HiveIcon, text: 'Custom' }

  const elements = [
    {
      tag: 'span',
      text: text,
      attributes: { class: text !== 'Custom' ? 'font-medium' : '' }
    },
    {
      tag: 'span',
      text: text === 'Custom' ? 'Json' : 'Custom Json'
    }
  ]

  let contractNameAction = ''
  if (['ssc-mainnet-hive', 'ssc-testnet-hive'].includes(payload.id)) {
    if (Array.isArray(json)) {
      contractNameAction = json.length !== 1 ? `${json.length} contracts` : `${json[0].contractName}.${json[0].contractAction}`
    } else {
      contractNameAction = `${json.contractName}.${json.contractAction}`
    }
  }

  // RC delegation JSON
  let rc_shifted: any[]
  if (payload.id === 'rc') {
    let delegatees = ''
    let delegatedRc = 0

    // json with wrapped only one array
    if (json.length === 1) {
      json = json[0]
    }

    if (!Array.isArray(json[0])) {
      payload.id = json[0]
      json[1].delegatees.length > 1 ? (delegatees = '@multiple accounts') : (delegatees = json[1].delegatees[0])
      delegatedRc = json[1].max_rc
    } else {
      payload.id = json[0][0]
      delegatees = '@multiple accounts'
      delegatedRc = json.map((amount: any) => amount[1].max_rc).reduce((acc: number, curr: number) => acc + curr, 0)
    }

    delegatees === '@multiple accounts'
      ? (rc_shifted = [
          {
            tag: 'span',
            text: delegatees,
            attributes: { class: 'text-sm' }
          },
          {
            tag: 'span',
            text: ' | ',
            attributes: { class: `text-sm` }
          },
          {
            tag: 'span',
            text: formatCurrency(delegatedRc, '0.000a'),
            attributes: { class: `text-sm` }
          }
        ])
      : (rc_shifted = [
          {
            tag: 'router-link',
            text: `@${delegatees} `,
            attributes: { to: `/@${delegatees}`, class: `${defaultLinkClass} text-sm` }
          },
          {
            tag: 'span',
            text: `| ${formatCurrency(delegatedRc, '0.000a')}`,
            attributes: { class: `text-sm` }
          }
        ])
  }

  const details = []
  if (payload.required_auths.length) {
    details.push('Auth: Active')
  }
  if (contractNameAction) {
    details.push(payload.contractNameAction)
  }

  return {
    icon: icon,
    elements: elements.concat([
      renderSeparator(),
      ...renderDefaultType(name, `${contractNameAction || payload.id}`),
      ...renderDefaultAccount(customJsonAccount)
    ]),
    shifted: rc_shifted || [],
    details: details
  }
}
