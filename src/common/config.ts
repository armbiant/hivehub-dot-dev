import { RouteRecordRaw } from 'vue-router'

const name = 'common'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/:pathMatch(.*)*',
    name: 'not-found',
    component: () => import('~/common/pages/NotFound.vue')
  }
]

export const config = {
  name: name,
  enabled: true, // this module cannot be disabled
  routes: routes
}
